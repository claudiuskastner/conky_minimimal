#! /usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

RED="\e[31m"
RESET="\e[m"
BOLD="\e[1m"
HOME_DIR="/home/${USER}"
CONKY_DIR="${HOME_DIR}/.conky"
DESKTOP_ENTRY_FILE="${HOME_DIR}/.config/autostart/conky.desktop"

# Check if conky is installed
command -v conky >/dev/null 2>&1 || {
    echo >&2 "conky is required but not installed"
    printf "please install ${BOLD}conky${RESET} with:\n${RED}$ apt-get install conky-all${RESET}\n"
    exit 1
}

# Create conky settings dir if not exists
if [ ! -d "${CONKY_DIR}" ]; then
    mkdir ${CONKY_DIR}
fi

# Link the conky.conf file in to the conky directory if "conky.conf" file exists
if [ ! -f "${CONKY_DIR}/conky.conf" ]; then
    ln -s "${PWD}/conky.conf" "${CONKY_DIR}/conky.conf"
else
    printf "The file ${BOLD}conky.conf${RESET} already exists.\n"
    read -p "Overwrite (y/n)?" choice
    case "$choice" in
    y | Y)
        echo "removing old conky.conf file ..  "
        rm "${CONKY_DIR}/conky.conf"
        echo "creating new conky.conf"
        ln -s "${PWD}/conky.conf" "${CONKY_DIR}/conky.conf"
        ;;
    n | N)
        echo "Could not install conky theme .. exiting"
        ;;
    *)
        echo "Invalid argument .. exiting"
        exit 0
        ;;
    esac
fi

if [ ! -d "${CONKY_DIR}/lua-scripts/" ]; then
    ln -s "${PWD}/lua-scripts" "${CONKY_DIR}/lua-scripts"
else
    printf "The file ${BOLD}conky.conf${RESET} already exists.\n"
    read -p "Overwrite (y/n)?" choice
    case "$choice" in
    y | Y)
        echo "removing old lua-scripts folder ..  "
        rm "${CONKY_DIR}/lua-scripts"
        echo "creating new lua-scripts"
        ln -s "${PWD}/lua-scripts" "${CONKY_DIR}/lua-scripts"
        ;;
    n | N)
        echo "Could not install conky theme .. exiting"
        ;;
    *)
        echo "Invalid argument .. exiting"
        exit 0
        ;;
    esac
fi

# Create autostart folder if not exists
if [ ! -d "${HOME_DIR}/.config/autostart" ]; then
    echo "Creating autostart folder"
    mkdir -p "${HOME_DIR}/.config/autostart"
fi

# Create conky.desktop entry
if [ ! -f "${CONKY_DIR}/.config/conky.desktop" ]; then
    echo "Create conky.desktop entry"
    cp "${PWD}/conky.desktop" ${DESKTOP_ENTRY_FILE}
    echo "Exec=conky -p10 -c ${CONKY_DIR}/conky.conf" >>${DESKTOP_ENTRY_FILE}

fi
