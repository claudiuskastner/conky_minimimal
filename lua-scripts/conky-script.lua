-- This is a lua script for calculating the cpu usage for every core

cpu_count = io.popen("nproc")
output = cpu_count:read("*a")

function conky_main ()
    local file = io.popen("nproc")
    local numcpus = file:read("*n")
    file:close()
    cpu_table = ""
    for i = 1,numcpus,2 do
        cpu_table = cpu_table.."Core "..i..": ".."${color white}${cpu cpu"..tostring(i).."}".."%${color black}".."${alignr}Core "..(i+1)..": ".."${color white}${cpu cpu"..tostring(i+1).."}".."%${color black}\n"
    end

    function conky_mycpus()
     return cpu_table
    end
end
