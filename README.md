# conky_minimimal

A simple and minimalistic conky theme.

- [conky_minimimal](#conkyminimimal)
  - [Screenshot](#screenshot)
  - [Installation](#installation)
    - [Prerequisites](#prerequisites)
    - [Installation script](#installation-script)
    - [Manual](#manual)
  - [Update](#update)

## Screenshot

![A screenshot of the conky theme](res/conky.png)

## Installation

### Prerequisites

You need conky installed.
Install with:

```bash
$ apt-get install conky-all
```

### Installation script

The install script creates a symbolic link in `$HOME/.conky/conky.conf` to `$PWD/conky.conf` and a startup entry in `$HOME/.config/autostart/conky.desktop`

Run the installation script:
```bash
$ chmod +x install.sh
$ ./install.sh
```

### Manual

Copy the `conky.conf` to ${HOME}/.conky/conky.conf

## Update

If you want to update this theme and used the installation script, just `git pull` this repo.

To manually update, copy the contents of `conky.conf` to `${HOME}/.conky/conky.conf`.
